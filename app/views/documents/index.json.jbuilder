json.array!(@documents) do |document|
  json.extract! document, :id, :title, :doc
  json.url document_url(document, format: :json)
end
