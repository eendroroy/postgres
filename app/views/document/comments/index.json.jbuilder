json.array!(@document_comments) do |document_comment|
  json.extract! document_comment, :id, :document_id, :comment
  json.url document_comment_url(document_comment, format: :json)
end
