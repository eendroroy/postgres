class CommentsController < ApplicationController
  before_action :set_comments
  before_action :set_comment, only: [:show, :edit, :update, :destroy]

  # GET documents/1/comments
  def index
    @comments = @document.comments
  end

  # GET documents/1/comments/1
  def show
  end

  # GET documents/1/comments/new
  def new
    @comment = @document.comments.build
  end

  # GET documents/1/comments/1/edit
  def edit
  end

  # POST documents/1/comments
  def create
    @comment = @document.comments.build(comment_params)

    if @comment.save
      redirect_to([@comment.document, @comment], notice: 'Comment was successfully created.')
    else
      render action: 'new'
    end
  end

  # PUT documents/1/comments/1
  def update
    if @comment.update_attributes(comment_params)
      redirect_to([@comment.document, @comment], notice: 'Comment was successfully updated.')
    else
      render action: 'edit'
    end
  end

  # DELETE documents/1/comments/1
  def destroy
    @comment.destroy

    redirect_to document_comments_url(@document)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comments
      @document = Document.find(params[:document_id])
    end

    def set_comment
      @comment = @document.comments.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def comment_params
      params.require(:comment).permit(:document_id, :comment)
    end
end
