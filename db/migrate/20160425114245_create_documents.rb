class CreateDocuments < ActiveRecord::Migration
  def change
    create_table :documents do |t|
      t.string :title
      t.jsonb :doc

      t.timestamps null: false
    end
    execute "CREATE INDEX document_doc_name_index ON documents USING GIN ((doc->'name'));"
  end
end
