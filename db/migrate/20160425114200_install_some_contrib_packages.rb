class InstallSomeContribPackages < ActiveRecord::Migration
  def up
    execute 'CREATE EXTENSION IF NOT EXISTS btree_gin;'
    execute 'CREATE EXTENSION IF NOT EXISTS btree_gist;'
    execute 'CREATE EXTENSION IF NOT EXISTS plpgsql;'
    execute 'CREATE EXTENSION IF NOT EXISTS pg_trgm;'
    execute 'CREATE EXTENSION IF NOT EXISTS fuzzystrmatch;'
  end

  def down
    execute 'DROP EXTENSION IF EXISTS btree_gin;'
    execute 'DROP EXTENSION IF EXISTS btree_gist;'
    execute 'DROP EXTENSION IF EXISTS plpgsql;'
    execute 'DROP EXTENSION IF EXISTS pg_trgm;'
    execute 'DROP EXTENSION IF EXISTS fuzzystrmatch;'
  end
end