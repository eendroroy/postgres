class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.references :document, foreign_key: true
      t.jsonb :comment

      t.timestamps null: false
    end
    # execute "CREATE INDEX comment_index ON comments USING gin (comment)"
  end
end
